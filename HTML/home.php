<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../css/sito.css">
    <script src="https://kit.fontawesome.com/ff2ae393e7.js" crossorigin="anonymous"></script>
    <script src="../js/jquery-3.5.0.min.js"></script>
    <script src="../js/sito.js"></script>
    <script src="../js/jquery.cycle.all.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="icon" type="image/png" sizes="16x16" href="../media/Favicon.png">
    <title>Closest | Vendi e compra prodotti realizzati da designer, li produrremo vicino a casa tua!</title>
  </head>

  <body>
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_collapse" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse " id="navbar_collapse">
        <ul class="navbar-nav d-flex align-items-center justify-content-between w-100">
          <a class="navbar-brand d-flex justify-content-center" href="home.html">
            <img src="../media/CLOSEST_W.png" alt="CLOSEST logo">
          </a>
          <div class=" mx-auto" id="search_bar_navbar">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Cerca">
              <div class="input-group-append">
                <button class="btn btn-orange" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </div>
            </div><!--fine input-group -->
          </div>
          <li class="nav-item">
            <a class="nav-link" href="vendi_noAccount.html">Vendi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="catalogo.html">Catalogo</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="help.html">Aiuto!!</a>
          </li>
          <li class="nav-item nav-item-hidden">
            <a class="nav-link" href="come_funziona.html">Come funziona</a>
          </li>
          <li class="nav-item nav-item-hidden">
            <a class="nav-link" href="mission.html">Mission</a>
          </li>
          <li class="nav-item nav-item-hidden">
            <a class="nav-link" href="team.html">Team</a>
          </li>
          <li class="nav-item" id="carrello_icon">
            <a class="nav-link" href="carrello.html"><i class="fas fa-shopping-cart"></i></a>
          </li>
          <li class="nav-item" id="account_icon">
            <a class="nav-link" href="crea_account.html"><i class="fas fa-user"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <section class="text-center" id="section_mobile">
        <h2><b>Cosa stai cercando?</b></h2>
        <div class="d-flex justify-content-center mx-auto">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Cerca">
            <div class="input-group-append">
              <button class="btn btn-secondary" type="button">
                <i class="fa fa-search"></i>
              </button>
            </div>
          </div>
        </div>
      </section>
      <section class="text-center" id="section_desktop">
        <h2><b>Benvenuto in CLOSEST</b></h2>
        <h5>Vendi e compra prodotti realizzati da designer, li produrremo vicino a casa tua!</h5>
        <a class="btn btn-orange mr-2" href="vendi.html" role="button">Vendi</a>
        <a class="btn btn-orange" href="catalogo.html" role="button">Vai al catalogo</a>
      </section>

      <div class="container" id="container_home">
        <div class="row">
          <div class="col-md-4">
            <div class="card mb-4">
              <a href="prodotto.html">
                <img class="bd-placeholder-img card-img-top"  src="../media/lampada_3.jpg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img">
              </a>
              <div class="card-body">
                <h6 class="d-inline font-weight-bold">Lampada da tavolo</h6>
                <br>
                <small><a href="#">By Prodes</a></small>
                <div class="d-flex justify-content-between align-items-center mt-4">
                  <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-outline-secondary">Aggiungi al carrello</button>
                  </div>
                  <h5 class="d-inline text-right txt-orange font-weight-bold mb-0">60 €</h5>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card mb-4">
              <a href="prodotto.html">
                <img class="bd-placeholder-img card-img-top"  src="../media/lampada_3.jpg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img">
              </a>
              <div class="card-body">
                <h6 class="d-inline font-weight-bold">Lampada da tavolo</h6>
                <br>
                <small><a href="#">By Prodes</a></small>
                <div class="d-flex justify-content-between align-items-center mt-4">
                  <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-outline-secondary">Aggiungi al carrello</button>
                  </div>
                  <h5 class="d-inline text-right txt-orange font-weight-bold mb-0">60 €</h5>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card mb-4">
              <a href="prodotto.html">
                <img class="bd-placeholder-img card-img-top"  src="../media/lampada_3.jpg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img">
              </a>
              <div class="card-body">
                <h6 class="d-inline font-weight-bold">Lampada da tavolo</h6>
                <br>
                <small><a href="#">By Prodes</a></small>
                <div class="d-flex justify-content-between align-items-center mt-4">
                  <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-outline-secondary">Aggiungi al carrello</button>
                  </div>
                  <h5 class="d-inline text-right txt-orange font-weight-bold mb-0">60 €</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer class="w-100 bg-dark">
      <div class="row">
        <div class="col-sm">
        </div>
        <div class="col-md">
          <ul class="list-unstyled text-small">
            <li><a class="text-muted" href="vendi.html">Vendi</a></li>
            <li><a class="text-muted" href="catalogo.html">Catalogo</a></li>
            <li><a class="text-muted" href="come_funziona.html">Come funziona</a></li>
            <li><a class="text-muted" href="mission.html">Mission</a></li>
            <li><a class="text-muted" href="team.html">Team</a></li>
          </ul>
        </div>
        <div class="col-md">
          <ul class="list-unstyled text-small text-muted">
            <img src="../media/CLOSEST_W.png" alt="CLOSEST logo">
            <li>Via Asiago 2/D, Vimercate (MB), Italy</li>
            <li>Codice Fiscale NSSLRT95C04F205V</li>
          </ul>
        </div>
        <div class="col-sm">
        </div>
      </div>
    </footer>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
