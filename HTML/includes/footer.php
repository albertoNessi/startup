</div>
<footer class="w-100 bg-dark">
  <div class="row">
    <div class="col-sm">
    </div>
    <div class="col-md">
      <ul class="list-unstyled text-small">
        <li><a class="text-muted" href="vendi.html">Vendi</a></li>
        <li><a class="text-muted" href="catalogo.html">Catalogo</a></li>
        <li><a class="text-muted" href="come_funziona.html">Come funziona</a></li>
        <li><a class="text-muted" href="mission.html">Mission</a></li>
        <li><a class="text-muted" href="team.html">Team</a></li>
      </ul>
    </div>
    <div class="col-md">
      <ul class="list-unstyled text-small text-muted">
        <img src="../media/CLOSEST_W.png" alt="CLOSEST logo">
        <li>Via Asiago 2/D, Vimercate (MB), Italy</li>
        <li>Codice Fiscale NSSLRT95C04F205V</li>
      </ul>
    </div>
    <div class="col-sm">
    </div>
  </div>
</footer>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>

?>
