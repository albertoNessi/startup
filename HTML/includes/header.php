<?php

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../css/sito.css">
    <script src="https://kit.fontawesome.com/ff2ae393e7.js" crossorigin="anonymous"></script>
    <script src="../js/jquery-3.5.0.min.js"></script>
    <script src="../js/sito.js"></script>
    <script src="../js/jquery.cycle.all.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="icon" type="image/png" sizes="16x16" href="../media/Favicon.png">
    <title>Closest | Vendi e compra prodotti realizzati da designer, li produrremo vicino a casa tua!</title>
  </head>

  <body>
