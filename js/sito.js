// carica_prodotto
function deleteComponent() {
  var xDelete = $(event.target);
  // var xDelete = $(event.target);
  xDelete.parent().remove();
};

var componentCount = 0;
var rowCount = 0;
var singleCount = 0;

function addComponent(){
  var divComponent = $("[id^='divComponent_']").last();

  componentCount++;
  singleCount++;

  var newNameComponent = "<div class='form-group mr-4' style='width: 50%''><label for='inputNameComponent_" + singleCount + "'>Nome</label><input type='text' class='form-control' id='inputNameComponent_" + singleCount + "'></div>";

  var newMaterialComponent = "<div class='form-group mr-4' style='width: 50%''><label for='InputMaterialComponent_" + singleCount + "'>Materiale</label><input type='text' class='form-control' id='InputMaterialComponent_" + singleCount + "'></div>";

  var newFileComponent = "<div class='form-group'><label for='InputComponentFile_" + singleCount + "'>File</label><input type='file' accept='.3fm, .stl' class='form-control' id='inputFileComponent_" + singleCount + "' multiple></div>";

  var xComponent = "<button class='btn form-group d-flex align-items-end pl-2 pr-0' id='deleteComponent_" + singleCount + "' onclick='deleteComponent()'>Rimuovi</button>";

  var newComponent = "<div class='form-group justify-content-between' id='divComponent_" + componentCount + "'><div class='d-flex justify-content-between'>" + newNameComponent + newMaterialComponent + newFileComponent + xComponent + "</div></div>";

  divComponent.after(newComponent);
}

// crea_account_produttore
function addTech(){
  var rowNow = $("[id^=rowProduzione_]").last();
  rowCount++;
  singleCount++;

  var newDivTech = "<div class='col-md mb-4' id='divTech_" + singleCount + "'><small>Tecnologia di produzione</small><div class='input-group mb-2 mt-1'><input type='text' class='form-control'><div class='input-group-append'><button class='btn btn-outline-danger' type='button' id='btnRemoveTech_" + singleCount + "' onclick='removeTech()'><i class='fas fa-times'></i></button></div></div></div>";

  var newDivMaterial = "<div class='col-md mb-2'><small>Materiali relativi a questa tecnologia</small><div class='input-group mb-2 mt-1' id='divMaterial_" + singleCount + "'><input type='text' class='form-control'></div><button type='button' class='btn btn-sm btn-outline-secondary mr-4' onclick='addMaterial(event)'' id='btnAddMaterial_" + singleCount + "'>Aggiungi Materiale</button></div>";

  var specifiche = "<div class='col-md-3' id='divSpecifiche_" + singleCount +"'><small>Specifiche componenti</small><div class='input-group mb-2 mt-1'><div class='input-group-prepend'><span class='input-group-text'>X</span></div><input type='number' class='form-control' placeholder='Larghezza massima' aria-label='Larghezza massima componenti' id='maxLarghezza_" + singleCount + "'><div class='input-group-append'><span class='input-group-text'>cm</span></div></div><div class='input-group mb-2'><div class='input-group-prepend'><span class='input-group-text'>Y</span></div><input type='number' class='form-control' placeholder='Profondità massima' aria-label='Profondità massima componenti' id='maxProfondità_" + singleCount + "'><div class='input-group-append'><span class='input-group-text'>cm</span></div></div><div class='input-group'><div class='input-group-prepend'><span class='input-group-text'>Z</span></div><input type='number' class='form-control' placeholder='Altezza massima' aria-label='Altezza massima componenti' id='maxAltezza_" + singleCount + "'><div class='input-group-append'><span class='input-group-text'>cm</span></div></div></div>";

  var newTech = "<hr><div class='row mb-4' id='rowProduzione_" + rowCount + "'>" + newDivTech + newDivMaterial + specifiche + "</div>";

  rowNow.after(newTech);
}

function addMaterial(event){
  singleCount++;

    var newMaterial = "<div class='input-group mb-2 mt-1' id='divMaterial_" + singleCount + "'><input type='text' class='form-control'><div class='input-group-append' id='btnRemoveMaterial_" + singleCount + "'><button class='btn btn-outline-danger' type='button' onclick='removeMaterial()'><i class='fas fa-times'></i></button></div></div>";


    $(event.target).before(newMaterial);
  // });
}

function removeTech(){
  $("[id^=btnRemoveTech_]").click(function(){
     $(this).closest("[id^=rowProduzione_]").remove();
  });
}

function removeMaterial(){
  $("[id^=btnRemoveMaterial_]").closest("[id^=divMaterial_]").remove();
}
